/*******************************************************************************
**
** Copyright (C) 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Aurora OS Application Template project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

//import QtQuick 2.7
import QtQuick.Layouts 1.0

Page {
    objectName: "mainPage"
    allowedOrientations: Orientation.Portrait

    id: window
        visible: true
//        visibility: "Windowed"
        width: /*gameGrid.width*/ parent.width
        height: mainCol.height
//        color: "grey"
//        title: "MineAI"

        QtObject {
            id: game
//            property var difficulties: {
//                "easy" : [8, 8, 10],
//                "advanced": [16, 16, 40],
//                "expert":   [26, 16, 86],
//                "test":     [5, 5, 24]
//            }

            // todo: maybe set these also on startGame
//            property var selectedDifficulty: difficulties["easy"]

            // must always match the current game, stop game if changed ?!
            property int rows: /*selectedDifficulty[0]*/ 8
            property int columns: /*selectedDifficulty[1]*/ 8
            property int mines: /*selectedDifficulty[2]*/ 10

            property int flagsLeft // set on startGame
            property int unrevealedFields // dito
            onUnrevealedFieldsChanged: {
//                console.log(unrevealedFields)
            }

            property bool started: false
            property bool won: !lost && unrevealedFields - mines === 0
            property bool lost: steppedOnMineIndex != -1
            property bool ended: won || lost
            property int steppedOnMineIndex: -1
        }

        SilicaFlickable {
            id: flicker
            anchors.fill: parent
//            contentHeight: game.columns * 100
//            contentWidth: game.rows * 100
//            ScrollDecorator { flickable: flicker }
            PushUpMenu {
                MenuItem {
                    text: "easy 8x8 - 10" /*qsTr("Load query")*/
                    onClicked: {
//                        game.selectedDifficulty = /*game.difficulties["easy"]*/
                        game.rows = /*game.selectedDifficulty[0]*/ 8
                        game.columns = /*game.selectedDifficulty[1]*/ 8
                        game.mines = /*game.selectedDifficulty[2]*/ 10
                        game.started = false
                        game.steppedOnMineIndex = -1
                        startGame()
                    }
                }
                MenuItem {
                    text: "advanced 16x16 - 40" /*qsTr("Load query")*/
                    onClicked: {
//                        game.selectedDifficulty = /*game.difficulties["advanced"]*/
                        game.rows = /*game.selectedDifficulty[0]*/ 8
                        game.rows = /*game.selectedDifficulty[0]*/ 16
                        game.columns = /*game.selectedDifficulty[1]*/ 8
                        game.columns = /*game.selectedDifficulty[1]*/ 16
                        game.mines = /*game.selectedDifficulty[2]*/ 10
                        game.mines = /*game.selectedDifficulty[2]*/ 40
                        game.started = false
                        game.steppedOnMineIndex = -1
                        startGame()
                    }
                }
                MenuItem {
                    text: "expert 26x16 - 86" /*qsTr("Load query")*/
                    onClicked: {
//                        game.selectedDifficulty = /*game.difficulties["expert"]*/
                        game.rows = /*game.selectedDifficulty[0]*/ 8
                        game.rows = /*game.selectedDifficulty[0]*/ 26
                        game.columns = /*game.selectedDifficulty[1]*/ 8
                        game.columns = /*game.selectedDifficulty[1]*/ 16
                        game.mines = /*game.selectedDifficulty[2]*/ 40
                        game.mines = /*game.selectedDifficulty[2]*/ 86
                        game.started = false
                        game.steppedOnMineIndex = -1
                        startGame()
                    }
                }
                MenuItem {
                    text: "test 5x5 - 24" /*qsTr("Load query")*/
                    onClicked: {
//                        game.selectedDifficulty = /*game.difficulties["test"]*/
                        game.rows = /*game.selectedDifficulty[0]*/ 5
                        game.columns = /*game.selectedDifficulty[1]*/ 5
                        game.mines = /*game.selectedDifficulty[2]*/ 24
                        game.started = false
                        game.steppedOnMineIndex = -1
                        startGame()
                    }
                }
            }
//        }

        Column {
            id: mainCol
            RowLayout {
                width: gameGrid.width
                Text {
                    color: Theme.highlightColor
                    text: "Time"
                }

                Button {
                    Layout.fillWidth: true
                    text: if (game.lost) {
                              "X("
                          } else if (game.won) {
                              "8-)"
                          } else {
                              ":)"
                          }

                    onClicked: {
                        startGame();
                    }
                }

                Text {
                    color: Theme.highlightColor
                    text: game.flagsLeft
                }
            }
            GridLayout {
                id: gameGrid
                columns: game.columns
                width: window.width
                rowSpacing: 0
                columnSpacing: 0

                Repeater {
                    id: fieldRepeater
                    model: game.columns * game.rows

                    Rectangle {
                        property bool mine
                        property bool flagged
                        property bool revealed
                        property int adjacentMines
                        property int adjacentFlags
                        property int adjacentHidden // i.e. flagged or untouched
                        width: /*100*/ window.width/ game.columns
                        height: width

                        color:
                            if (game.steppedOnMineIndex === index) {
                                "red"
                            } else if (revealed || (game.lost && mine)){
                                "black"
                            } else {
                                "green"
                            }

                        border {
                            color: "black"
                            width: 2
                        }

//                        Text {
//                            text: parent.adjacentHidden
//                            font.pixelSize: 10
//                            color: "white"
//                        }

                        Text {
                            anchors.fill: parent
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            text:
                                if (parent.flagged || (game.won && parent.mine)) {
                                    "F";
                                } else if (parent.revealed) {
                                    if (mine) {
                                        "X";
                                    } else {
                                        if (parent.adjacentMines > 0) {
                                            parent.adjacentMines
                                        } else {
                                            ""
                                        }
                                    }
                                } else if (game.lost && mine){
                                    "M"
                                } else {
                                    ""
                                }

                            color:
                                switch (text) {
                                case "1": return "#05ff03";
                                case "2": return "white";
                                case "3": return "#0cffff"
                                case "4": return "#ffff03";
                                case "5": return "#0800ff";
                                case "6": return "pink";
                                case "7": return "pink";
                                case "F": return "#ff0101";
                                default: return "white";
                                }

                        }

                        MouseArea {
                            anchors.fill: parent
                            acceptedButtons: Qt.LeftButton | Qt.RightButton
                            onClicked: {
                                console.log("clicked")
                                if (game.ended || parent.revealed) {
                                    return
                                }
                                if (mouse.button & Qt.RightButton) {
                                    toggleFlag()
                                } else if (!parent.flagged){
                                    reveal();
                                }
                            }
                            onPressAndHold: {
                                console.log("holded")
                                toggleFlag()
                            }
                        }

                        function toggleFlag() {
                            flagged = !flagged;
                            var flagDelta = flagged ? 1 : -1
                            game.flagsLeft -= flagDelta

                            // --- AI reveal if flag matches numbers ---
//                            updateNeighborsByIndex(index, function(field) {
//                                field.adjacentFlags += flagDelta
//                                field.aiFlagMatchNumber();
//                            });
                        }

                        function aiFlagMatchNumber() {
                            if (!revealed || adjacentFlags !== adjacentMines) {
                                return;
                            }
                            updateNeighborsByIndex(index, function (field) {
                               if (!field.flagged && !field.revealed) {
                                   field.reveal()
                               }
                            });
                        }

                        function aiHiddenMatchNumber() {
                            if (!revealed ||
                                    adjacentHidden !== adjacentMines) {
                                return;
                            }
                            updateNeighborsByIndex(index, function (field) {
                               if (!field.flagged && !field.revealed) {
                                   field.toggleFlag()
                               }
                            });
                        }

                        function reveal() {
                            if (flagged || revealed || game.ended) {
                                return;
                            }
                            revealed = true
                            game.unrevealedFields--
                            console.warn(game.unrevealedFields)
                            if (mine) {
                                game.steppedOnMineIndex = index;
                            } else if (adjacentMines === 0) {
                                updateNeighborsByIndex(index, function(field) {
                                    if (field.flagged) {
                                        return;
                                    }
                                    field.reveal();
                                });
                            }
                            updateNeighborsByIndex(index, function (field) {
                                field.adjacentHidden--;
                                field.aiHiddenMatchNumber();
                            })
                            aiFlagMatchNumber();
                        }
                    }
                }
            }

            Component.onCompleted: {
                startGame()
            }

        }
}

        function startGame() {
            for (var i = 0; i < game.columns * game.rows; i++) {
                var field = fieldRepeater.itemAt(i)
                field.mine = false
                field.revealed = false
                field.flagged = false
                field.adjacentMines = 0
                field.adjacentFlags = 0

                var x = i % game.columns;
                var y = Math.floor(i / game.columns);
                var adjacentHidden = 8
                if (x === 0 || x === game.columns - 1) {
                    adjacentHidden -= 3;
                }
                if (y === 0 || y === game.rows - 1) {
                    adjacentHidden -= 3;
                }
                field.adjacentHidden = Math.max(adjacentHidden, 3) // account for corner
            }
            game.flagsLeft = game.mines
            game.unrevealedFields = game.rows * game.columns
            game.started = false
            game.steppedOnMineIndex = -1

            for (var mine = 0; mine < game.mines; mine++) {
                while (true) {
                    x = getRandomInt(game.columns);
                    y = getRandomInt(game.rows);
                    if (!getField(x, y).mine) {
                        getField(x, y).mine = true
                        updateNeighbors(x, y, function(field) {
                            field.adjacentMines++;
                        });
                        break;
                    }
                }
            }
        }
        function updateNeighborsByIndex(index, f) {
            var x = index % game.columns;
            var y = Math.floor(index / game.columns);
            updateNeighbors(x, y, f);
        }

        function updateNeighbors(x, y, f) {
            for (var xd = -1; xd <= 1; xd++) {
                for (var yd = -1; yd <= 1; yd++) {
                    if (xd == 0 && yd == 0) {
                        continue;
                    }
                    var field = getField(x + xd, y + yd);
                    if (field) {
                        f(field);
                    }
                }
            }
        }

        function getField(x, y) {
            if (x < 0 || x >= game.columns || y < 0 || y >= game.rows) {
                return null;
            }

            return fieldRepeater.itemAt(x + game.columns * y)
        }

        function getRandomInt(max) {
            return Math.floor(Math.random() * Math.floor(max));
        }
}
